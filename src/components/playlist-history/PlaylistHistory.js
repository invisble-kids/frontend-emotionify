import React from 'react';
import History from './History/History';

class PlaylistHistory extends React.Component {
    

    componentDidMount() {
        fetch('http://127.0.0.1:8000/profile/', {
            method: 'GET',
            headers: { 'Authorization': 'Token '.concat(localStorage.getItem('token')) }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
        })
        .catch(err => console.log(err))
    }

    render() {
        return (
            <div id="#history">
                <History />
            </div>
        )
    }
    
}

export default PlaylistHistory;