import React, { Component } from 'react';
import './RadioButtons.css';

class RadioButtons extends Component {
    state = {
        emotion: this.props.emotionObject.emotionTag,
        mappingEmotion: null,
    }

    handleChange = (event) => {
        var updatedChoice = { emotion: this.state.emotion, mappingEmotion: event.target.value}
        console.log(updatedChoice)
        this.props.update_choices(updatedChoice)
    }

    make_radio_buttons = (optionsItem) => {
        return (
            <form className="form">
                <div className="row">
                    <div className="col s8 m10 l10 offset-s2 offset-m1 offset-l2" style={{textAlign:"left"}}>
                    {optionsItem.map((mappingEmotion, mappingIndex) => {
                        return (
                            <p key={mappingIndex}>
                            <label className="black-text">
                                <input 
                                    value={mappingEmotion.mappingName} 
                                    onChange={this.handleChange}
                                    className="radio-btn" 
                                    name="group1"
                                    type="radio"/>
                                <span>{mappingEmotion.emotionName} music</span>
                            </label>
                            </p>
                        )
                    })}
                    </div>
                </div>
            </form>
        )
    }

    render() {
        var { emotionObject } = this.props;
        return (
            <div  style={{verticalAlign:"center", marginTop:"2vh"}}>
                {this.make_radio_buttons(emotionObject.options[0])}
                <h6 style={{textAlign:"", margin:"0vw 0vw 1vw 0vw"}}>and</h6>
                {this.make_radio_buttons(emotionObject.options[1])}
            </div>
        );
    }
}


export default RadioButtons;
