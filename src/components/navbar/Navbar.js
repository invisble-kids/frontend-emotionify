import React from 'react';
import {Link} from 'react-router-dom';
import Sidebar from './Sidebar';
import SignedInNav from './SignedInNav';
import SignedOutNav from './SignedOutNav';
import './Navbar.css';

class Navbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            menu: false,
            auth: localStorage.getItem('auth')
        };
        this.toggleMenu = this.toggleMenu.bind(this);
    }

    toggleMenu() {
        this.setState({menu: !this.state.menu})
    }

    render() {

        var authenticated = this.state.auth ? <SignedInNav/> : <SignedOutNav/>;

        return (
            <div>
                <Sidebar/>
                <nav className="nav">
                    <div className="nav-wrapper blue-grey darken-4">
                        <div className="container">

                            <Link to="" data-target="slide-out" className="sidenav-trigger"><i
                                className="material-icons">menu</i></Link>

                            <Link to="/" className="brand-logo" style={{padding:"0px 10px"}}>Emotionify</Link>

                            <ul className="right hide-on-med-and-down">
                                {authenticated}
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}

export default Navbar;
                