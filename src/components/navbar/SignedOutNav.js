import React from 'react';
import { Link } from 'react-router-dom';

class SignedOutNav extends React.Component {
    render() {
        return (
            <div>
                <Link to="/sign-in" className="btn teal accent-4 waves-effect waves-light">
                    Connect to Spotify
                </Link>
            </div>

        );
    }
    
}

export default SignedOutNav;
                