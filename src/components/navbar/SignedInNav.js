import React from 'react';
import {Link} from 'react-router-dom';
import M from 'materialize-css';

class SignedInNav extends React.Component {
    componentDidMount() {
        var elems = document.querySelectorAll('.dropdown-trigger');
        M.Dropdown.init(elems);
    }

    handleLogOut = () => {
        localStorage.removeItem("token");
        localStorage.removeItem("auth");
        window.location.assign('/')
    }


    render() {
        return (
            <div>
                <ul className="left">
                    <li>
                       <a className="waves-effect waves-light" href="/profile">
                            <i class="medium material-icons">face</i>
                       </a>
                    </li>
                    <li>
                        <a className="waves-effect waves-light" onClick={() => this.handleLogOut()}>
                            <i class="medium material-icons">exit_to_app</i>
                        </a>
                    </li>
                </ul>
            </div>

        );
    }
}

export default SignedInNav;
                