import React from 'react';
import PlaylistHistory from '../playlist-history/PlaylistHistory';
import EmotionPreference from '../emotion-preference/EmotionPreference';
import M from 'materialize-css';

class Profile extends React.Component {
    
    componentDidMount() {
        M.Tabs.init(this.Tabs);
    }
    
    render() {
        return (
            <div>
                <div className="row">
                    <div>
                        <ul className="tabs blue-grey darken-3" ref={Tabs => {this.Tabs = Tabs;}} >
                            <li className="tab col s3"><a href="#history" className="white-text active">History Profile</a></li>
                            <li className="tab col s3"><a href="#preference" className="white-text">Emotion Preference</a></li>
                        </ul>
                    </div>
                    
                    <div
                        className="z-depth-1 blue-grey lighten-5 col s12 m8 l8 offset-m2 offset-l2 black-text"
                        style={{display: "inline-block", marginTop:"2%", padding:"4%"}}
                    >
                        <div id="history"><PlaylistHistory /></div>
                        <div id="preference"><EmotionPreference /></div>
                    </div>
                    
                </div>
            </div>
        )
    }
}

export default Profile;