import React, {Component} from "react";

class SignIn extends Component {
    state = {
        redirect: null,
        code: null,
        status: null
    };

    componentDidMount() {
        let code = window.location.search;
        if (code) {
            this.setState({
                code: code
            });

            const upload = new FormData();
            upload.append("code", window.location.href);
            fetch('http://127.0.0.1:8000/spotify-connect/', {
                method: 'POST',
                body: upload
            })
            .then(response => {
                if (!response.ok) {
                    throw response.statusText;
                }
                return response
            })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                localStorage.setItem('token', data.token);
                localStorage.setItem('auth', true);
                window.location.assign('/select');
                this.setState({
                    status: 'pass'
                })
            })
            .catch(err => {
                this.setState({
                    status: 'failed'
                })
                console.log(err)
            })
        } else {
            fetch('http://127.0.0.1:8000/spotify-connect/', {
                method: 'GET',
                headers: {'Content-Type': 'application/json',}
            })
            .then(response => {
                if (!response.ok) {
                    throw response.statusText;
                }
                return response
            })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({
                    redirect: data.register_url,
                    status: 'pass'
                });
                window.location.assign(data.register_url);
            })
            .catch(err => {
                this.setState({
                    status: 'failed'
                })
                console.log(err)
            })
        }
    }

    render() {
        var result = this.state.status === null ? (
                <div class="progress">
                    <div class="indeterminate"></div>
                </div>
            ) : (
                this.state.status === 'failed' ? (
                    <h1 className="component">something is wrong!</h1>
                ) : (
                    <div class="progress">
                        <div class="indeterminate"></div>
                    </div>
                )
            )

        return (
            <center>
                <div className="container center">
                    <div className="row">
                        <div class="z-depth-1 grey lighten-4 col s8 m8 l8 offset-l2 offset-m2 offset-s2" style={{display: "inline-block", marginTop:"5%", padding:"4%"}}>
                            {result}
                        </div>
                    </div>
                </div>
            </center>
        )
    }
}

export default SignIn;