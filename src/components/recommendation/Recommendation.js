import React from 'react';
import Player from '../Player/Player';
import {Link} from 'react-router-dom';
import './Recommendation.css';

class Recommendation extends React.Component {
    state = {
        data: null ,
        playlist_uri: null ,
        status: null ,
    }

    componentDidMount() {
        const upload = new FormData();
        if (this.props.location.data) {
            this.setState({
                data: this.props.location.data.fetchData,
            }, () => {
            upload.append(this.props.location.data.type , this.state.data[0]);
            let header = {};
            if (localStorage.getItem('auth')){
               header = { 'Authorization': 'Token '.concat(localStorage.getItem('token')) }
            }
            fetch(this.props.location.data.fetchURL , {
                method: 'POST',
                body: upload ,
                headers: header
            })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({
                    playlist_uri: data.playlist_id,
                    status: 'pass'
                })
            })
            .catch(err => {
                this.setState({
                    status: 'failed'
                })
                console.log(err)
            })
            
            })
        }
        
    }

    render() {
        console.log(this.props.location.data)
        var result = this.state.status === null ? (
            <center>
                <div className="container center">
                    <div className="row">
                            <div class="progress">
                                <div class="indeterminate"></div>
                        </div>
                    </div>
                </div>
            </center>
        ) : (
            this.state.status === 'pass' ? (
                <Player playlist_uri={this.state.playlist_uri} />
            ) : (
                <h1 className="component">something is wrong!</h1>
            )
        )
        return (
            
            <div className="row">
                {this.state.data ? (
                    <div
                        className="z-depth-1 blue-grey lighten-5 col s12 m8 l8 offset-m2 offset-l2 black-text"
                        style={{display: "inline-block", marginTop:"2%", padding:"4%"}}
                    >
                        {result}
                    </div>
                ) : (
                    <div
                        className="z-depth-1 blue-grey lighten-5 col s12 m8 l8 offset-m2 offset-l2 black-text"
                        style={{display: "inline-block", marginTop:"2%", padding:"4%"}}
                    >
                        <Link to="/select" className="btn black white-text">Select Your Emotion First!</Link>
                    </div>
                )}
                
            </div>
        )
    }
    
}

export default Recommendation;
