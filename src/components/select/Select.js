import React from 'react';
import ImagePlaylist from '../image-playlist/ImagePlaylist';
import NaivePlaylist from '../naive-playlist/NaivePlaylist';

class Select extends React.Component {
    render() {
        return(
            <center>
                <div className="container">
                    <div className="row">
                        <div class="z-depth-1 blue-grey lighten-5 col s12 m8 l8 offset-m2 offset-l2" style={{display: "inline-block", marginTop:"2%", padding:"4%"}}>
                            <ImagePlaylist/>
                            <hr />
                            <NaivePlaylist/>
                        </div>
                    </div>
                </div>
            </center>
        )
    }
}

export default Select;