import React from 'react';
import AddPlaylist from './options/AddPlaylist';
import LikeTracks from './options/LikeTracks';
import './Player.css';

class Player extends React.Component {
    state = {
        playlist_uri: this.props.playlist_uri,
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            playlist_uri: newProps.playlist_uri,
        })
    }

    style = (width) => {
        if (width >= 600){
            return ({
                width: width*0.585 ,
                height: 1100 ,
            })
        }
        else {
            return ({
                width: width*0.92 ,
                height: 1100 ,
            })
        }
    }

    render() {
        console.log(this.state.playlist_uri)
        var dataList;
        if (this.state.playlist_uri !== null ) {
            console.log(this.state.playlist_uri)
            var playlistSize = this.style(window.innerWidth);
            var player = "https://open.spotify.com/embed/playlist/".concat(this.state.playlist_uri);
            dataList = (
                    <iframe title="playlist" src={player} width={playlistSize.width} height={playlistSize.height}
                            frameBorder="0" allowtransparency="true" allow="encrypted-media" />
            )
        }
        
        return (
            <div className="player">
                <AddPlaylist playlist_uri={this.state.playlist_uri} />
                {dataList}
                <LikeTracks playlist_uri={this.state.playlist_uri} />
            </div>
        )
    }
    
}

export default Player;
