import React from 'react';
import './LikeTracks.css';
import M from 'materialize-css';

class LikeTracks extends React.Component {
    state = {
        playlist_uri: null,
        like_buttons: [],
        dislike_buttons: []
    }

    componentDidMount() {
        this.setState(
            {
                playlist_uri: this.props.playlist_uri
            },
            () => this.show_buttons()
        )
    }

    componentWillReceiveProps(newProps) {
        if (this.state.playlist_uri !== newProps.playlist_uri) {
            this.setState(
                {
                    playlist_uri: newProps.playlist_uri
                },
                () => this.show_buttons()
            )
        }
    }

    show_buttons = () => {
        var like_array = new Array();
        var dislike_array = new Array();
        if (!localStorage.getItem('auth')) {
            for (var i = 0; i < 20; i++) {
                like_array.push("far fa-heart")
                dislike_array.push("far fa-thumbs-down")

                this.setState({
                    like_buttons: like_array,
                    dislike_buttons: dislike_array,
                })
            }
            return
        }

        var uploadData = new FormData;
        uploadData.append("playlist_id", this.state.playlist_uri)
        fetch("http://127.0.0.1:8000/liked-tracks/", {
            method: "POST",
            body: uploadData,
            headers: {'Authorization': 'Token '.concat(localStorage.getItem('token'))}
        })
            .then(response => response.json())
            .then(answer => {
                console.log(answer)
                for (var i = 0; i < 20; i++) {
                    if (answer.liked_tracks_index.includes(i))
                        like_array.push("fas fa-heart")
                    else
                        like_array.push("far fa-heart")
                }
                console.log(answer)
                for (var i = 0; i < 20; i++) {
                    if (answer.disliked_tracks_index.includes(i))
                        dislike_array.push("fas fa-thumbs-down")
                    else
                        dislike_array.push("far fa-thumbs-down")
                }
                this.setState({
                    like_buttons: like_array,
                    dislike_buttons: dislike_array,
                })
            })
            .catch(err => console.log(err))

    }

    like_dislike_track(index, method) {
        if (!localStorage.getItem('auth')) {
            M.toast({
                html: 'You must login to like or dislike tracks!',
            })
            return
        }

        if (method === "POST" && this.state.like_buttons[index] === "fas fa-heart") {
            M.toast({
                html: 'You already liked that track!',
            })
            return
        }

        if (method === "DELETE" && this.state.dislike_buttons[index] === "fas fa-thumbs-down") {
            M.toast({
                html: 'You already disliked that track!',
            })
            return
        }

        var uploadData = new FormData();
        uploadData.append("playlist_id", this.props.playlist_uri)
        uploadData.append("track_index", index)
        fetch("http://127.0.0.1:8000/like/track/", {
            method: method,
            body: uploadData,
            headers: {'Authorization': 'Token '.concat(localStorage.getItem('token'))}
        })
            .then(response => {
                if (!response.ok) {
                    throw response.statusText;
                }
                return response
            })
            .then(answer => {
                console.log(answer)
                let likes = this.state.like_buttons
                let dislikes = this.state.dislike_buttons

                if (method === "POST") {
                    likes[index] = "fas fa-heart"
                    dislikes[index] = "far fa-thumbs-down"
                } else if (method === "DELETE") {
                    likes[index] = "far fa-heart"
                    dislikes[index] = "fas fa-thumbs-down"
                }
                this.setState({
                    like_buttons: likes,
                    dislike_buttons: dislikes,
                })
            })
            .catch(err => {
                M.toast({
                    html: err,
                })
                console.log(err)
            })
    }

    render() {
        var CountTracks = Array.from(Array(20).keys());

        var likeButtons = (
            CountTracks.map((item, index) => {
                return (
                    <div key={index} className="tuple">
                        <a className="like" onClick={() => this.like_dislike_track(item, "POST")}>
                            <i className={this.state.like_buttons[item]}/>
                        </a>
                        <a className="dislike" onClick={() => this.like_dislike_track(item, "DELETE")}>
                            <i className={this.state.dislike_buttons[item]} aria-hidden="true"/>
                        </a>
                    </div>

                )
            }));

        return (
            <div className="buttons">
                {likeButtons}
            </div>
        )
    }
}

export default LikeTracks;
