import React from 'react';
import './ImagePlaylist.css';
import { Redirect } from 'react-router-dom';
import upload_image_icon from '../close-up.svg';

class EmotionPlaylist extends React.Component {
    state = {
        image: null ,
    }

    set_image = (image) => {
        this.setState({
            image: image ,
        })
    }

    renderRedirect = () => {
        if (this.state.image !== null) {
            return (
                <Redirect
                to={{
                    pathname:"recommendation",
                    data:
                        {
                            fetchData: this.state.image,
                            fetchURL: 'http://127.0.0.1:8000/recommend/image/',
                            type: "image"
                        }
                }}
                />   
            )
        }
    }

    render() {
        
        return (
            <center>
                <div>
                    {this.renderRedirect()}
                    <div className="row black-text">
                        <p className="col s10 offset-s1">Let us know your emotion with a photo of your face.</p>
                        <p className="col s10 offset-s1">You can choose or take your picture using button below.</p>
                        <div className="col s8 m8 l8 offset-l2 offset-m2 offset-s2" style={{display: "inline-block", padding:"4%"}}>
                        <center>
                            <img src={upload_image_icon} className="take-photo-img" />

                            <label className="take-photo">TAKE PHOTO
                            <input 
                                className="file-input" type="file"
                                onChange={e => this.set_image(e.target.files)} multiple
                            />
                            </label>   
                        </center>
                        </div>
                    </div>
                </div>
            </center>
        )
    }
    
}

export default EmotionPlaylist;