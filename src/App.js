import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import './App.css';
import './laptop.css';
import './mobile.css';

import Home from './components/home/Home';
import Navbar from './components/navbar/Navbar';
import SignIn from './components/Authentication/Sign-in';
import ImagePlaylist from './components/image-playlist/ImagePlaylist'
import Profile from './components/profile/Profile';
import NaivePlaylist from './components/naive-playlist/NaivePlaylist';
import Select from './components/select/Select';
import EmotionPreference from './components/emotion-preference/EmotionPreference';
import Recommendation from './components/recommendation/Recommendation';

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>

        <div className="App" >
          <Navbar />

          <Route exact path='/' component={Home} />
          <Route path='/sign-in' component={SignIn} />
          <Route path='/profile' component={Profile} />
          <Route path='/image-playlist' component={ImagePlaylist} />
          <Route path='/naive-playlist' component={NaivePlaylist} />
          <Route path='/select' component={Select} />
          <Route path='/emotion-preference' component={EmotionPreference} />
          <Route path='/recommendation' component={Recommendation} />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
