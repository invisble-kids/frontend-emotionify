const express = require('express');
const path = require('path');

const app = express();
const static_path = path.join(__dirname, '..', 'build');
app.use(express.static(static_path));

app.get('*', (req,res) =>{
    res.sendFile(path.join(static_path, 'index.html'));
});

const port = process.env.PORT || 3000;
app.listen(port);
console.log('App is listening on port ' + port);